/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.proyectoejemplopoo;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author admin
 */
public class Main {

    public static void main(String[] args) {

        Alumno alumnoAdrian = new Alumno(2, "Teleco", false, "all00023", "12345678A", "Adrian Luque", 30);
        EmpleadoPAS pasAdrian = new EmpleadoPAS(23700, "Informatica", "61234567", "987654321", "12346578A", "Adrian Luque", 30);
        EmpleadoPDI pdiAdrian = new EmpleadoPDI("D-151", args, "alluque", "12346578A", "Adrian Luque", 30);

        System.out.println(alumnoAdrian.getIdentificador());
        System.out.println(pasAdrian.getIdentificador());
        System.out.println(pdiAdrian.getIdentificador());
        
        ArrayList<Alumno> alumnos = new ArrayList<>();
        alumnos.add(new Alumno(2, "Teleco", false, "bll00023", "12345678A", "Adrian Luque", 30));
        alumnos.add(new Alumno(2, "Teleco", false, "all00023", "12345678A", "Adrian Luque", 28));
        alumnos.add(new Alumno(2, "Teleco", false, "all00023", "12345678A", "Adrian Luque", 38));
        alumnos.add(new Alumno(2, "Teleco", false, "all00023", "12345678A", "Adrian Luque", 19));
        alumnos.add(new Alumno(2, "Teleco", false, "all00023", "12345678A", "Adrian Luque", 45));
        
        Collections.sort(alumnos);
        
        for (Alumno alumno : alumnos) {
            System.out.println(alumno.getEdad());
        }
        
        System.out.println(alumnoAdrian.toString());
        
    }

}
