/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.proyectoejemplopoo;

import java.io.Serializable;

/**
 *
 * @author admin
 */
public class Alumno extends Persona implements Comparable<Alumno>, Serializable{
    
    int curso;
    String grado;
    boolean graduado;
    String usuarioAlumno;
    private String nombre;

    public Alumno(int curso, String grado, boolean graduado, String usuarioAlumno, String dni, String nombre, int edad) {
        super(dni, nombre, edad);
        this.curso = curso;
        this.grado = grado;
        this.graduado = graduado;
        this.usuarioAlumno = usuarioAlumno;
    }

    @Override
    public String getIdentificador() {
        return usuarioAlumno;
    }

    @Override
    public int compareTo(Alumno o) {
        
        return usuarioAlumno.compareTo(o.usuarioAlumno);

    }

    public int getEdad() {
        return edad;
    }
    
    

    
    
}
