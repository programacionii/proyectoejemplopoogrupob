/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.proyectoejemplopoo;

/**
 *
 * @author admin
 */
public class EmpleadoPDI extends Persona{
    
    String despacho;
    String asignaturas[];
    String usuarioProfesor;
    
    @Override
    public String getIdentificador() {
        return usuarioProfesor; //To change body of generated methods, choose Tools | Templates.
    }

    public EmpleadoPDI(String despacho, String[] asignaturas, String usuarioProfesor, String dni, String nombre, int edad) {
        super(dni, nombre, edad);
        this.despacho = despacho;
        this.asignaturas = asignaturas;
        this.usuarioProfesor = usuarioProfesor;
    }
    
   
    
}
