/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.proyectoejemplopoo;

/**
 *
 * @author admin
 */
public abstract class Persona {
    
    private String dni;
    protected String nombre;
    int edad;

    public Persona() {
    }
    
    

    public Persona(String dni, String nombre, int edad) {
        this.dni = dni;
        this.nombre = nombre;
        this.edad = edad;
        
    }
    
    public boolean isMayorDeEdad(){
        return edad >=18;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public abstract String getIdentificador();       
    
}
